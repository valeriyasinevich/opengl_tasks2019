#include <Application.hpp>
#include <Framebuffer.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>
#include <sstream>
#include <vector>
#include "common/Mesh.hpp"
#include "MazeCameraMover.hpp"



struct LightInfo
{
    glm::vec3 position;
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    float attenuation;
    glm::vec3 coneDirection;
    float coneAngle;
};

class MazeApplication : public Application
{
public:
    MazeApplication(Maze &maze)
            : Application(std::make_shared<MazeCameraMover>(maze)), _camera_active(_cameraMover),
              _camera_inactive(std::make_shared<MazeCameraMover>(maze, true)) {
        localMaze = &maze;
    }
    std::vector<MeshPtr> _cubes;
    MeshPtr _sphere;
    MeshPtr _ground;

    static const int LN = 2;
    LightInfo _lights[LN];
    int _active_light = 0;

    MeshPtr mazeMesh;
    Maze* localMaze;
    ShaderProgramPtr shader;
    ShaderProgramPtr _markerShader;
    std::shared_ptr<CameraMover> _camera_active;
    std::shared_ptr<CameraMover> _camera_inactive;
    GLuint _sampler;
    TexturePtr _textures[2];
    MeshPtr _marker;
    MeshPtr _quad;

    //Идентификатор шейдерной программы
    ShaderProgramPtr _quadDepthShader;
    ShaderProgramPtr _quadColorShader;
    ShaderProgramPtr _renderToShadowMapShader;
    ShaderProgramPtr _renderToGBufferShader;
    ShaderProgramPtr _renderDeferredShader;

    //Переменные для управления положением одного источника света
    float _lr = 10.0;
    float _phi = 0.0;
    float _theta = 0.48;

    LightInfo _light;
    CameraInfo _lightCamera;

    GLuint _depthSampler;

    bool _applyEffect = true;
    bool _applyGammaTexture = true;

    bool _showGBufferDebug = false;
    bool _showShadowDebug = false;
    bool _showDeferredDebug = false;

    FramebufferPtr _gbufferFB;
    TexturePtr _depthTex;
    TexturePtr _normalsTex;
    TexturePtr _diffuseTex;

    FramebufferPtr _shadowFB;
    TexturePtr _shadowTex;

    FramebufferPtr _deferredFB;
    TexturePtr _deferredTex;

    //Старые размеры экрана
    int _oldWidth = 1024;
    int _oldHeight = 1024;

    void initFramebuffers()
    {
        //Создаем фреймбуфер для рендеринга в G-буфер

        _gbufferFB = std::make_shared<Framebuffer>(1024, 1024);

        _normalsTex = _gbufferFB->addBuffer(GL_RGB16F, GL_COLOR_ATTACHMENT0);
        _diffuseTex = _gbufferFB->addBuffer(GL_RGB8, GL_COLOR_ATTACHMENT1);
        _depthTex = _gbufferFB->addBuffer(GL_DEPTH_COMPONENT16, GL_DEPTH_ATTACHMENT);

        _gbufferFB->initDrawBuffers();

        if (!_gbufferFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================
        //Создаем фреймбуфер для рендеринга в теневую карту

        _shadowFB = std::make_shared<Framebuffer>(1024, 1024);

        _shadowTex = _shadowFB->addBuffer(GL_DEPTH_COMPONENT16, GL_DEPTH_ATTACHMENT);

        _shadowFB->initDrawBuffers();

        if (!_shadowFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================
        //Создаем фреймбуфер для результатов расчета освещения

        _deferredFB = std::make_shared<Framebuffer>(1024, 1024);

        _deferredTex = _deferredFB->addBuffer(GL_RGB8, GL_COLOR_ATTACHMENT0);

        _deferredFB->initDrawBuffers();

        if (!_deferredFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }
    }

    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей
// mazeMesh = makeCube(1   );//makeMaze(*localMaze);
        mazeMesh = makeMaze(*localMaze);
        //mazeMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.5f)));

        //фонарик
        _lights[0] = {
                glm::vec3(0.0, 0.0, 1.f),   //position
                glm::vec3(0.3, 0.3, 0.3),   //ambient
                glm::vec3(0.5, 0.3, 0.0),   //diffuse
                glm::vec3(1.0, 1.0, 1.0),   //specular
                0.0,                        //attenuation
                glm::vec3(0.0, 0.0, -1.0),  //direction
                15.0                        //cone angle
        };
        //лампа
        _lights[1] = {
                glm::vec3(10.0, 10.0, 3.0), //position
                glm::vec3(0.3, 0.3, 0.3),  //ambient
                glm::vec3(0.7, 0.7, 0.7),  //diffuse
                glm::vec3(1.0, 1.0, 1.0),  //specular
                0.0,                       //attenuation
                glm::vec3(0.0, 0.0, 1.0), //direction
                180.0                      //cone angle
        };
        //std::cout << "here" << std::endl;

        shader = std::make_shared<ShaderProgram>("/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/shaderLights.vert",
                                                  "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/shaderLights.frag");

        _markerShader = std::make_shared<ShaderProgram>("/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/marker.vert",
                                                        "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/marker.frag");

        const std::string texFilenames[2] = {
                "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/stone_wall.jpg", "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/stone_wall_normal.jpg"
        };
        for(int i = 0; i < 2; ++i){
            _textures[i] = loadTexture(texFilenames[i]);
        }

        _marker = makeSphere(0.1f);

        _cubes.resize(4);
        _cubes[0] = makeCube(0.3, glm::vec3(9, 9, 1.5));
        _cubes[0]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        _cubes[1] = makeCube(0.3, glm::vec3(7, 9, 1.5));
        _cubes[1]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        _cubes[2] = makeCube(0.5, glm::vec3(7, 7, 1));
        _cubes[2]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        _cubes[3] = makeCube(0.2, glm::vec3(5, 7, 1));
        _cubes[3]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        _sphere = makeSphere(0.5f);
        _sphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _ground = makeGroundPlane(5.0f, 2.0f);

        _quad = makeScreenAlignedQuad();

        //=========================================================
        //Инициализация шейдеров
        //std::cout << "here" << std::endl;

        _quadDepthShader = std::make_shared<ShaderProgram>("/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/quadDepth.vert", "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/quadDepth.frag");
        _quadColorShader = std::make_shared<ShaderProgram>("/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/quadColor.vert", "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/quadColor.frag");
        _renderToShadowMapShader = std::make_shared<ShaderProgram>("/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/toshadow.vert", "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/toshadow.frag");
        _renderToGBufferShader = std::make_shared<ShaderProgram>("/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/shaderLights.vert",
                                                                 "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/shaderLights.frag");
        _renderDeferredShader = std::make_shared<ShaderProgram>("/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/quadColor.vert", "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/deferred.frag");
        //=========================================================
        //Инициализация значений переменных освщения
        //std::cout << "here" << std::endl;
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        GLfloat border[] = { 1.0f, 0.0f, 0.0f, 1.0f };

        glGenSamplers(1, &_depthSampler);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glSamplerParameterfv(_depthSampler, GL_TEXTURE_BORDER_COLOR, border);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

        //=========================================================
        glfwGetFramebufferSize(_window, &_oldWidth, &_oldHeight);

        //=========================================================
        //Инициализация фреймбуфера для рендера теневой карты

        initFramebuffers();
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }

            ImGui::Checkbox("Apply gamma", &_applyEffect);
            ImGui::Checkbox("Use SRGB texture", &_applyGammaTexture);
            ImGui::Checkbox("Show G-buffer debug", &_showGBufferDebug);
            ImGui::Checkbox("Show shadow debug", &_showShadowDebug);
            ImGui::Checkbox("Show deferred debug", &_showDeferredDebug);
        }
        ImGui::End();
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_1)
            {
                _applyEffect = !_applyEffect;
            }
            else if (key == GLFW_KEY_2)
            {
                _applyGammaTexture = !_applyGammaTexture;
            }
            else if (key == GLFW_KEY_Z)
            {
                _showGBufferDebug = !_showGBufferDebug;
            }
            else if (key == GLFW_KEY_X)
            {
                _showShadowDebug = !_showShadowDebug;
            }
            else if (key == GLFW_KEY_C)
            {
                _camera_active.swap(_camera_inactive);
                _cameraMover = _camera_active;
            }
            if (key == GLFW_KEY_E)
            {
                _active_light = (_active_light + 1) % LN;
            }
        }
    }

    void update()
    {
        Application::update();

        _lightCamera.viewMatrix = glm::lookAt(_lights[_active_light].position, glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        _lightCamera.projMatrix = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 30.f);
        double dt = glfwGetTime() - _oldTime;

        glm::mat4 matrix = mazeMesh->modelMatrix();

        mazeMesh->setModelMatrix(matrix);
        //Если размер окна изменился, то изменяем размеры фреймбуферов - перевыделяем память под текстуры
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        if (width != _oldWidth || height != _oldHeight)
        {
            _gbufferFB->resize(width, height);
            _deferredFB->resize(width, height);

            _oldWidth = width;
            _oldHeight = height;
        }
    }

    void draw() override
    {
        //Рендерим геометрию сцены в G-буфер
        drawToGBuffer(_gbufferFB, _renderToGBufferShader, _camera);

        //Рендерим геометрию сцены в теневую карту с позиции источника света
        drawToShadowMap(_shadowFB, _renderToShadowMapShader, _lightCamera);

        //Выполняем отложенное освещение, заодно накладывает тени, а результат записываем в текстуру
        drawDeferred(_deferredFB, _renderDeferredShader, _camera, _lightCamera);

        //Выводим полученную текстуру на экран, попутно применяя эффект постобработки
        drawToScreen(_quadColorShader);

    }

    void drawToGBuffer(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader->use();
        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);

        shader->setVec3Uniform("light.pos", glm::vec3(camera.viewMatrix * glm::vec4(_lights[_active_light].position, -1.0)));
        shader->setVec3Uniform("lightPos", glm::vec3(camera.viewMatrix * glm::vec4(_lights[_active_light].position, -1.0)));

        if(_active_light == 0){
            shader->setVec3Uniform("light.pos", glm::vec3(-2.5,-3.5,0) + dynamic_cast<MazeCameraMover*>(_camera_inactive.get())->position());
            shader->setVec3Uniform("lightPos", dynamic_cast<MazeCameraMover*>(_camera_inactive.get())->position());
        }
        shader->setVec3Uniform("light.La", _lights[_active_light].ambient);
        shader->setVec3Uniform("light.Ld", _lights[_active_light].diffuse);
        shader->setVec3Uniform("light.Ls", _lights[_active_light].specular);
        shader->setFloatUniform("light.k", _lights[_active_light].attenuation);
        shader->setFloatUniform("light.coneAngle", _lights[_active_light].coneAngle);
        shader->setVec3Uniform("light.coneDirection", _lights[_active_light].coneDirection);

        shader->setVec3Uniform("material.Ka", glm::vec3(1.0, 1.0, 1.0));
        shader->setVec3Uniform("material.Kd", glm::vec3(1.0, 1.0, 1.0));
        shader->setVec3Uniform("material.Ks", glm::vec3(0.5, 0.5, 0.5));
        shader->setFloatUniform("material.shininess", 100.0);

        shader->setMat3Uniform("localToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix
                                                                                             * mazeMesh->modelMatrix()))));
        shader->setMat4Uniform("modelMatrix", mazeMesh->modelMatrix());
        for (int i = 0; i < 2; ++i) {
            glActiveTexture(GL_TEXTURE0 + i);
            glBindSampler(i, _sampler);
            _textures[i]->bind();
            if( i % 2 == 0) {
                shader->setIntUniform("diffuseTex", i);
            } else {
                shader->setIntUniform("normalTex", i);
            }
        }

        drawScene(shader, camera);

        glUseProgram(0); //Отключаем шейдер

        fb->unbind(); //Отключаем фреймбуфер
    }

    void drawToShadowMap(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& lightCamera)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_DEPTH_BUFFER_BIT);

        shader->use();
        shader->setMat4Uniform("lightViewMatrix", lightCamera.viewMatrix);
        shader->setMat4Uniform("lightProjectionMatrix", lightCamera.projMatrix);

        glEnable(GL_CULL_FACE);
        glFrontFace(GL_CCW);
        glCullFace(GL_FRONT);

        drawScene(shader, lightCamera);

        glDisable(GL_CULL_FACE);

        glUseProgram(0);

        fb->unbind();
    }

    void drawDeferred(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera, const CameraInfo& lightCamera)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_COLOR_BUFFER_BIT);

        shader->use();
        shader->setMat4Uniform("viewMatrixInverse", glm::inverse(camera.viewMatrix));
        shader->setMat4Uniform("projMatrixInverse", glm::inverse(camera.projMatrix));

        glm::vec3 lightPosCamSpace = glm::vec3(camera.viewMatrix * glm::vec4(_light.position, 1.0));

        shader->setVec3Uniform("light.La", _lights[_active_light].ambient);
        shader->setVec3Uniform("light.Ld", _lights[_active_light].diffuse);
        shader->setVec3Uniform("light.Ls", _lights[_active_light].specular);
        shader->setFloatUniform("light.k", _lights[_active_light].attenuation);
        shader->setFloatUniform("light.coneAngle", _lights[_active_light].coneAngle);
        shader->setVec3Uniform("light.coneDirection", _lights[_active_light].coneDirection);

        shader->setMat4Uniform("lightViewMatrix", lightCamera.viewMatrix);
        shader->setMat4Uniform("lightProjectionMatrix", lightCamera.projMatrix);

        glm::mat4 projScaleBiasMatrix = glm::scale(glm::translate(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5)), glm::vec3(0.5, 0.5, 0.5));
        shader->setMat4Uniform("lightScaleBiasMatrix", projScaleBiasMatrix);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _normalsTex->bind();
        shader->setIntUniform("normalsTex", 0);

        glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
        glBindSampler(1, _sampler);
        _diffuseTex->bind();
        shader->setIntUniform("diffuseTex", 1);

        glActiveTexture(GL_TEXTURE2);  //текстурный юнит 2
        glBindSampler(2, _sampler);
        _depthTex->bind();
        shader->setIntUniform("depthTex", 2);

        glActiveTexture(GL_TEXTURE3);  //текстурный юнит 3
        glBindSampler(3, _depthSampler);
        _shadowTex->bind();
        shader->setIntUniform("shadowTex", 3);

        _quad->draw(); //main light

        glUseProgram(0);

        fb->unbind();
    }

    void drawToScreen(const ShaderProgramPtr& shader)
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader->use();

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _deferredTex->bind();
        shader->setIntUniform("tex", 0);

        //glEnable(GL_FRAMEBUFFER_SRGB); //Альтернатива шейдеру с постобработкой

        _quad->draw();

        //glDisable(GL_FRAMEBUFFER_SRGB);

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void drawScene(const ShaderProgramPtr& shader, const CameraInfo& camera)
    {
        shader->setMat4Uniform("modelMatrix", mazeMesh->modelMatrix() );
        shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _ground->modelMatrix()))));

        mazeMesh->draw();
        for(int i = 0; i < _cubes.size(); ++i){
            shader->setMat4Uniform("modelMatrix", _cubes[i]->modelMatrix());
            shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _ground->modelMatrix()))));
            _cubes[i]->draw();
        }
        if (_active_light == 1) {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix",
                                          camera.projMatrix * camera.viewMatrix * glm::translate(glm::mat4(1.0f),
                                                                                                   _lights[1].position));
            _markerShader->setVec4Uniform("color", glm::vec4(_lights[1].diffuse, 1.0f));
            _marker->draw();

        }


    }
};

int main()
{
    Maze maze("/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/mazeSample.txt");
    MazeApplication app(maze);
    app.start();

    return 0;
}