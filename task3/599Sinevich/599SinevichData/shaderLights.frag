#version 330

struct LightInfo
{
    vec3 pos; //положение источника света в системе координат камеры
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света
    vec3 Ls; //цвет и интенсивность бликового света
    float k; //коэффициент ослабления
    vec3 coneDirection; //направление света
    float coneAngle; //полуугол конуса
};
uniform LightInfo light;
in VS_OUT {
    vec3 FragPos;
    vec2 TexCoords;
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} fs_in;
struct MaterialInfo
{
    vec3 Ka; //коэффициент отражения окружающего света
    vec3 Kd; //коэффициент отражения диффузного света
    vec3 Ks; //коэффициент отражения бликового света
    float shininess;
};
uniform MaterialInfo material;

uniform mat3 localToCameraMatrix;

uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in mat3 cameraToTangentMatrix;
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
in mat3 TBN;
in vec3 centralPointDir;
out vec4 fragColor; //выходной цвет фрагмента

void main()
{
    vec3 diffuseColor = texture(diffuseTex, texCoord).rgb; // кирпичная стеночка
    vec3 viewDirection = normalize(-posCamSpace.xyz);



    vec3 lightDirCamSpace = (light.pos.xyz - posCamSpace.xyz); //направление на источник света

    float distance = length(lightDirCamSpace); // расстояние до него
    lightDirCamSpace = normalize(lightDirCamSpace); // нормируем вектор

    vec3 normalTanSpace = normalize(texture(normalTex, texCoord).rgb * 2.0 - 1.0);
   // vec3 normalTanSpace = texture(normalTex, texCoord).rgb;
    fragColor = vec4(normalTanSpace,  1 );
   // vec3 normalCamSpace = normalize( TBN * localToCameraMatrix * normalTanSpace);

    vec3 normalCamSpace = normalize( localToCameraMatrix * TBN  * normalTanSpace);
    fragColor = vec4(normalCamSpace,  1 );

    float NdotL = max(dot(normalCamSpace, lightDirCamSpace.xyz), 0.0);
    vec3 colAmbient = diffuseColor * light.La * material.Ka;

    vec3 colDiffuse = diffuseColor * light.Ld * material.Kd * NdotL;
    fragColor = vec4(colDiffuse, 0.5);

    vec3 colSpecular = vec3(0.0, 0.0, 0.0);

    float angle = degrees(acos(dot(viewDirection, normalize(lightDirCamSpace))));

    if (NdotL > 0.0)
    {
        vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
        vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

        float blinnTerm = max(dot(normalCamSpace, halfVector), 0.0); //интенсивность бликового освещения по Блинну
        blinnTerm = pow(blinnTerm, 128.0); //регулируем размер блика

        colSpecular = light.Ls * material.Ks * blinnTerm;

    }

    float attenuation = 1.0 / (1.0 + light.k * pow(distance, 2));

    attenuation /= (1 + exp(angle - light.coneAngle));


    vec3 color = colAmbient + attenuation * (colDiffuse + colSpecular);

    fragColor = vec4(color, 0.5);
}
