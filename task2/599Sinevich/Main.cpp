#include "MazeCameraMover.hpp"

#include <Application.hpp>
#include <ShaderProgram.hpp>
#include <Mesh.hpp>
#include <Texture.hpp>


/**
Проект №3: Лабиринт
*/

struct LightInfo
{
    glm::vec3 position;
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    float attenuation;
    glm::vec3 coneDirection;
    float coneAngle;
};

class MazeApplication : public Application {
protected:

    static const int LN = 2;
    LightInfo _lights[LN];
    int _active_light = 1;

    MeshPtr mazeMesh;
    Maze* localMaze;
    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;
    std::shared_ptr<CameraMover> _camera_active;
    std::shared_ptr<CameraMover> _camera_inactive;
    GLuint _sampler;
    TexturePtr _textures[2];
    MeshPtr _marker;

public:
    MazeApplication(Maze &maze)
            : Application(std::make_shared<MazeCameraMover>(maze)), _camera_active(_cameraMover),
              _camera_inactive(std::make_shared<MazeCameraMover>(maze, true)) {
            localMaze = &maze;
    }

    ~MazeApplication() {}

    void makeScene() override {
        Application::makeScene();

       // mazeMesh = makeCube(1   );//makeMaze(*localMaze);
        mazeMesh = makeMaze(*localMaze);
        //mazeMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.5f)));

        //фонарик
        _lights[0] = {
                glm::vec3(0.0, 0.0, 1.f),   //position
                glm::vec3(0.3, 0.3, 0.3),   //ambient
                glm::vec3(0.5, 0.3, 0.0),   //diffuse
                glm::vec3(1.0, 1.0, 1.0),   //specular
                0.0,                        //attenuation
                glm::vec3(0.0, 0.0, -1.0),  //direction
                15.0                        //cone angle
        };
        //лампа
        _lights[1] = {
                glm::vec3(10.0, 10.0, 3.0), //position
                glm::vec3(0.3, 0.3, 0.3),  //ambient
                glm::vec3(0.7, 0.7, 0.7),  //diffuse
                glm::vec3(1.0, 1.0, 1.0),  //specular
                0.0,                       //attenuation
                glm::vec3(0.0, 0.0, -1.0), //direction
                180.0                      //cone angle
        };

        _shader = std::make_shared<ShaderProgram>("/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/shaderLights.vert",
                                                  "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/shaderLights.frag");

        _markerShader = std::make_shared<ShaderProgram>("/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/marker.vert",
                                                        "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/marker.frag");

        const std::string texFilenames[2] = {
                "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/stone_wall.jpg", "/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599SinevichData/stone_wall_normal.jpg"
        };
        for(int i = 0; i < 2; ++i){
            _textures[i] = loadTexture(texFilenames[i]);
        }

        _marker = makeSphere(0.1f);
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glSamplerParameterf(_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4.0f);
    }

    void update() override {
        double dt = glfwGetTime() - _oldTime;

        glm::mat4 matrix = mazeMesh->modelMatrix();

        mazeMesh->setModelMatrix(matrix);

        Application::update();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //_lights[0].position = dynamic_cast<MazeCameraMover*>(_camera_inactive.get())->position();
        //_lights[0].coneDirection = dynamic_cast<MazeCameraMover*>(_camera_inactive.get())->direction();//glm::vec3(0.0, 0.0, -1.0);

        _shader->setVec3Uniform("light.pos", glm::vec3(_camera.viewMatrix * glm::vec4(_lights[_active_light].position, -1.0)));
        _shader->setVec3Uniform("lightPos", glm::vec3(_camera.viewMatrix * glm::vec4(_lights[_active_light].position, -1.0)));

        if(_active_light == 0){
            _shader->setVec3Uniform("light.pos", glm::vec3(-2.5,-3.5,0) + dynamic_cast<MazeCameraMover*>(_camera_inactive.get())->position());
            _shader->setVec3Uniform("lightPos", dynamic_cast<MazeCameraMover*>(_camera_inactive.get())->position());
        }
        _shader->setVec3Uniform("light.La", _lights[_active_light].ambient);
        _shader->setVec3Uniform("light.Ld", _lights[_active_light].diffuse);
        _shader->setVec3Uniform("light.Ls", _lights[_active_light].specular);
        _shader->setFloatUniform("light.k", _lights[_active_light].attenuation);
        _shader->setFloatUniform("light.coneAngle", _lights[_active_light].coneAngle);
        _shader->setVec3Uniform("light.coneDirection", _lights[_active_light].coneDirection);

        _shader->setVec3Uniform("material.Ka", glm::vec3(1.0, 1.0, 1.0));
        _shader->setVec3Uniform("material.Kd", glm::vec3(1.0, 1.0, 1.0));
        _shader->setVec3Uniform("material.Ks", glm::vec3(0.5, 0.5, 0.5));
        _shader->setFloatUniform("material.shininess", 400.0);

        _shader->setMat3Uniform("localToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix
                                                                                             * mazeMesh->modelMatrix()))));
        _shader->setMat4Uniform("modelMatrix", mazeMesh->modelMatrix());
        for (int i = 0; i < 2; ++i) {
            glActiveTexture(GL_TEXTURE0 + i);
            glBindSampler(i, _sampler);
            _textures[i]->bind();
            if( i % 2 == 0) {
                _shader->setIntUniform("diffuseTex", i);
            } else {
                _shader->setIntUniform("normalTex", i);
            }
        }

        //Рисуем первый меш
        mazeMesh->draw();
        if (_active_light == 1) {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix",
                                          _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f),
                                                                                                   _lights[1].position));
            _markerShader->setVec4Uniform("color", glm::vec4(_lights[1].diffuse, 1.0f));
            _marker->draw();
        }
        glBindSampler(0, 0);
        glUseProgram(0);


    }
    void handleKey(int key, int scancode, int action, int mods)
    {
        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_C)
            {
                _camera_active.swap(_camera_inactive);
                _cameraMover = _camera_active;
            }
            if (key == GLFW_KEY_E)
            {
                _active_light = (_active_light + 1) % LN;
            }
        }
        Application::handleKey(key, scancode, action, mods);
    }
};

int main() {
    Maze maze("/home/valeriyasin/Documents/graphon/opengl_tasks2019/task3/599SinevichData/599Sinevichata/mazeSample"
                      ".txt");
    MazeApplication app(maze);
    app.start();

    return 0;
}
